var express = require('express')
var router = express.Router()
const http = require('http')
var session = require('express-session')
var flash = require('connect-flash')
const bodyParser = require('body-parser')
router.use(flash())
var multer = require('multer')
const swaggerJsDoc = require('swagger-jsdoc')
const swaggerUi = require('swagger-ui-express')
// Extended: https://swagger.io/specification/#infoObject
const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'Inventario Frontend',
      description: 'Inventario Information',
      contact: {
        name: 'Fernando Hernandez'
      },
      servers: ['http://localhost:3000']
    }
  },
  apis: ['routes/index.js']
}
const swaggerDocs = swaggerJsDoc(swaggerOptions)
router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs))
/**
 * MANEJO DE IMAGENES EN EL FRONTEND
 */
var ipBackend = process.env.BACKEND_HOST_IP
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname)
  }
})
var upload = multer({ storage: storage })
/**
 * @swagger
 * /:
 *  get:
 *    description: view index of the page
 *    responses:
 *      '200':
 *        description: Redirect to index or home
 */
router.get('/', function (req, res, next) {
  if (req.session.name) {
    res.render('home', { title: 'Express' })
  } else {
    res.render('index', { title: 'Express' })
  }
})
/**
 * @swagger
 * /login:
 *  get:
 *    description: view login
 *    responses:
 *      '200':
 *        description: Redirect to login
 */
router.get('/login', function (req, res, next) {
  res.render('login', { title: 'Express' })
})
/**
 * @swagger
 * /signup:
 *  get:
 *    description: view login
 *    responses:
 *      '200':
 *        description: Redirect to signup
 */
router.get('/signup', function (req, res, next) {
  res.render('signup', { title: 'Express' })
})
/**
 * @swagger
 * /home:
 *  get:
 *    description: view home
 *    responses:
 *      '200':
 *        description: Redirect to home or login
 */
router.get('/home', function (req, res) {
  if (req.session.name) {
    res.render('home', { title: 'Express' })
  } else {
    res.redirect('/login')
  }
})

// Listar Vehiculos solo del ajustador
function getVehiculos (id, callback) {
  const options = {
    hostname: ipBackend,
    port: 80,
    path: '/Vehiculo_ajustador/token?id=' + id,
    method: 'GET'
  }
  var variable2 = ''

  const request = http.request(options, response => {
    response.on('data', function (chunk) {
      variable2 += chunk
    })
    response.on('end', function () {
      const result = JSON.parse(variable2)

      return callback(null, {
        result: response.statusCode,
        datos: result.response
      })
    })
  })
  request.end()
}
/**
 * @swagger
 * /list_vehiculos:
 *  get:
 *    description: List of Vehiculos
 *    responses:
 *      '200':
 *        description: Redirect to list_vehiculos or login
 */
router.get('/list_vehiculos', function (req, res, next) {
  let datos = ''
  if (req.session.name) {
    getVehiculos(req.session.user, function (err, data) {
      if (data.result == 200) {
        datos = data
        // console.log(data.datos);
        return res.render('list_vehiculos', { dato: datos.datos })
      } else {
        datos = data
        console.log(data.datos)
        return res.render('list_vehiculos', { dato: [] })
      }
    })
  } else {
    res.render('login')
  }
})

// Listar Vehiculos solo del ajustador
function putEstado (id, estado, callback) {
  const options = {
    hostname: ipBackend,
    port: 80,
    path: '/Vehiculo_ajustador/token/' + id + '?estado=' + estado,
    method: 'PUT'
  }
  var variable2 = ''

  const request = http.request(options, response => {
    response.on('data', function (chunk) {
      variable2 += chunk
    })
    response.on('end', function () {
      const result = JSON.parse(variable2)

      return callback(null, {
        result: response.statusCode,
        datos: result.response
      })
    })
  })
  request.end()
}
router.get('/estado/:id/:estado', function (req, res, next) {
  const id = req.params.id
  const estado = req.params.estado
  if (req.session.name) {
    putEstado(id, estado, function (err, data) {
      if (data.result == 200) {
        res.redirect('/list_vehiculos')
      } else {
        res.redirect('/list_vehiculos')
      }
    })
  } else {
    res.render('login')
  }
})
/**
 * @swagger
 * /Vehiculo:
 *  get:
 *    description: View form Vehiculos
 *    responses:
 *      '200':
 *        description: Redirect to form vehiculo
 */
router.get('/vehiculo', function (req, res, next) {
  if (req.session.name) {
    res.render('vehiculo')
  } else {
    res.render('login')
  }
})
function insertarVehiculo (data, callback) {
  // console.log(data);
  const options = {
    hostname: ipBackend,
    port: 80,
    path: '/Vehiculo/token',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': data.length
    }
  }
  let variable2 = ''
  const request = http.request(options, response => {
    response.on('data', function (chunk) {
      variable2 += chunk
    })
    response.on('end', function () {
      const result = JSON.parse(variable2)
      return callback(null, {
        result: response.statusCode,
        datos: result.response
      })
    })
  })
  request.write(data)
  request.end()
}
/**
 * @swagger
 * /Vehiculo:
 *  post:
 *    description: View form Vehiculos
 *    parameters:
 *      - name: tipo
 *        in: body
 *        description: tipo de vehiculo
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: marca
 *        in: body
 *        description: marca de vehiculo
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: linea
 *        in: body
 *        description: linea de vehiculo
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: modelo
 *        in: body
 *        description: modelo de vehiculo
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: placa
 *        in: body
 *        description: placa de vehiculo
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: color
 *        in: body
 *        description: color de vehiculo
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: numero_chasis
 *        in: body
 *        description: numero de chasis de vehiculo
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: numero_motor
 *        in: body
 *        description: numero de motor de vehiculo
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: tipo
 *        in: body
 *        description: tipo de vehiculo
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: precio_base
 *        in: body
 *        description: precio base de vehiculo
 *        required: true
 *        schema:
 *          type: number
 *          format: number
 *      - name: precio_minimo
 *        in: body
 *        description: precio minimo de vehiculo
 *        required: true
 *        schema:
 *          type: number
 *          format: number
 *      - name: arranca
 *        in: body
 *        description: el vehiculo arranca
 *        required: true
 *        schema:
 *          type: boolean
 *          format: boolean
 *      - name: camina
 *        in: body
 *        description: el vehiculo camina
 *        required: true
 *        schema:
 *          type: boolean
 *          format: boolean
 *      - name: falla_mecanica
 *        in: body
 *        description: el vehiculo presenta fallas
 *        required: true
 *        schema:
 *          type: boolean
 *          format: boolean
 *      - name: garantia
 *        in: body
 *        description: el vehiculo posee garantia de inspeccion
 *        required: true
 *        schema:
 *          type: boolean
 *          format: boolean
 *      - name: inundado
 *        in: body
 *        description: el vehiculo ha estado inudado
 *        required: true
 *        schema:
 *          type: boolean
 *          format: boolean
 *      - name: colision
 *        in: body
 *        description: el vehiculo presenta choques
 *        required: true
 *        schema:
 *          type: boolean
 *          format: boolean
 *      - name: observacion
 *        in: body
 *        description: observaciones generales del vehiculo
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *    responses:
 *      '200':
 *        description: Redirect to Seleccionar fotos
 */
router.post('/vehiculo', function (req, res, next) {
  const datos = JSON.stringify({
    estado: 1,
    tipo: req.body.tipo,
    marca: req.body.marca,
    linea: req.body.linea,
    modelo: req.body.modelo,
    placa: req.body.placa,
    color: req.body.color,
    numero_chasis: req.body.numero_chasis,
    numero_motor: req.body.numero_motor,
    ajustador: req.session.user,
    precio_base: req.body.precio_base,
    precio_minimo: req.body.precio_minimo,
    arranca: req.body.arranca == 'true',
    camina: req.body.camina == 'true',
    falla_mecanica: req.body.falla == 'true',
    garantia_inspeccion: req.body.garantia == 'true',
    inundado: req.body.inundado == 'true',
    colision: req.body.colision == 'true',
    observacion: req.body.observacion
  })
  if (req.session.name) {
    insertarVehiculo(datos, function (err, data) {
      if (data.result == 201) {
        req.flash('msg', 'Vehiculo Creado')
        res.locals.messages = req.flash()
        res.render('fotos', { placa: req.body.placa, ip_backend: ipBackend })
      } else {
        req.flash('msg', 'Datos no validos')
        res.locals.messages = req.flash()
        res.render('vehiculo')
      }
    })
  } else {
    res.render('login')
  }
})
// ver datos
router.get('/perfil', function (req, res) {
  if (req.session.name) {
    const options = {
      hostname: ipBackend,
      port: 80,
      path: '/Ajustador/token?id=' + 1,
      method: 'GET'
    }
    const request = http.request(options, response => {
      response.on('data', function (chunk) {
        const variable = JSON.parse(chunk)
        console.log(variable.response[0])
        res.render('profile', { data: variable.response[0] })
      })
    })
    request.end()
  } else {
    res.redirect('/login')
  }
})

// Login de usuario ajustador
let variable = ''
let variableUser = ''
let variableName = ''
function iniciarLogin (username, passwrd, callback) {
  const options = {
    hostname: ipBackend,
    port: 80,
    path: '/login/token?username=' + username + '&passwrd=' + passwrd,
    method: 'GET'
  }
  const request = http.request(options, response => {
    response.on('data', function (chunk) {
      if (response.statusCode == 200) {
        variable = JSON.parse(chunk)
        variableUser = variable.body[0].id_ajustador
        variableName = variable.body[0].username
      }
    })
    response.on('end', () => {
      return callback(null, {
        result: response.statusCode,
        id: variableUser,
        name: variableName
      })
    })
  })
  request.end()
}
/**
 * @swagger
 * /logueo:
 *  post:
 *    description: Login in Inventario
 *    parameters:
 *      - name: username
 *        in: body
 *        description: username de ajustador
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *      - name: password
 *        in: body
 *        description: password de ajustador
 *        required: true
 *        schema:
 *          type: string
 *          format: string
 *    responses:
 *      '200':
 *        description: Redirect to Home
 *      '404':
 *        description: Redirect to Login
 */
router.post('/logueo', function (req, res, next) {
  const username = req.body.username
  const passwrd = req.body.password
  iniciarLogin(username, passwrd, function (err, data) {
    if (data.result == 200) {
      req.flash('msg', 'Bienvenido!')
      res.locals.messages = req.flash()
      console.log(data.id + ' ' + data.name)
      req.session.name = data.name
      req.session.user = data.id
      req.session.save()
      res.render('home')
    } else {
      req.flash('msg', 'Crendenciales incorrectas!')
      res.locals.messages = req.flash()
      res.render('login')
    }
  })
})
// route for user logout
router.get('/logout', (req, res) => {
  if (req.session.name) {
    res.clearCookie('user_sid')
    req.flash('msg', 'Sesion Cerrada!')
    res.locals.messages = req.flash()
    res.render('index')
    req.session.destroy(function () {})
  } else {
    req.flash('msg', 'Sesion Cerrada!')
    res.locals.messages = req.flash()
    res.render('login')
  }
})
// creacion de cuenta de ajustador
router.post('/crear_cuenta', function (req, res, next) {
  const data = JSON.stringify({
    username: req.body.username,
    nombre: req.body.name,
    passwrd: req.body.password
  })
  console.log(data)
  const options = {
    hostname: ipBackend,
    port: 80,
    path: '/Ajustador/token',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': data.length
    }
  }
  const request = http.request(options, response => {
    if (response.statusCode == 201) {
      req.flash('msg', 'Cuenta Creada')
      res.locals.messages = req.flash()
      res.render('login')
    } else {
      req.flash('msg', 'Datos no validos')
      res.locals.messages = req.flash()
      res.render('signup')
    }
  })
  request.write(data)
  request.end()
})
function getFotos (id, callback) {
  const options = {
    hostname: ipBackend,
    port: 80,
    path: '/Foto_completo/token/' + id,
    method: 'GET'
  }
  let variable3 = ''

  const request = http.request(options, response => {
    response.on('data', function (chunk) {
      variable3 += chunk
    })
    response.on('end', function () {
      const result = JSON.parse(variable3)

      return callback(null, {
        result: response.statusCode,
        datos: result.response
      })
    })
  })
  request.end()
}
router.get('/conf', function (req, res, next) {
  getFotos(req.query.id, function (err, data) {
    if (data.result == 200) {
      req.flash('msg', 'Imagenes Subidas por favor configure su visibilidad')
      res.locals.messages = req.flash()
      console.log(data.datos)
      res.render('conf', { dato: data.datos, ip_backend: ipBackend })
    } else {
      res.render('home')
    }
  })
})

router.post('/subir_archivos', upload.any(), function (req, res, next) {
  console.log(req.files)
  for (let i = 0; i < req.files.length; i++) {
    const data = JSON.stringify({
      url: 'http://34.73.116.192:81/static/uploads/' + req.files[i].filename,
      externa: 0,
      id_vehiculo: req.body.id
    })
    console.log(data)
    const options = {
      hostname: ipBackend,
      port: 80,
      path: '/Foto/token',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Content-Length': data.length
      }
    }
    const request = http.request(options, response => {
      response.on('end', function () {})
    })
    request.write(data)
    request.end()
  }
  return res.redirect('conf?id=' + req.body.id)
})
router.get('/ver_adjudicacion', function (req, res, next) {
  getAdjudicacion(req.query.id, function (err, data) {
    if (data.result == 200) {
      res.render('ver_adjudicacion', { dato: data.datos})
    } else {
      res.render('home')
    }
  })
})
function getAdjudicacion (id, callback) {
  const options = {
    hostname: ipBackend,
    port: 80,
    path: '/Puja?jwt=token&id=' + id,
    method: 'GET'
  }
  let variable3 = ''

  const request = http.request(options, response => {
    response.on('data', function (chunk) {
      variable3 += chunk
    })
    response.on('end', function () {
      const result = JSON.parse(variable3)

      return callback(null, {
        result: response.statusCode,
        datos: result.response
      })
    })
  })
  request.end()
}
module.exports = router
