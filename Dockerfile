FROM node:10-alpine

# Create app directory
RUN mkdir /app
WORKDIR /app
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package.json ./
RUN npm install 
RUN npm install http-errors
RUN npm install -g nodemon
RUN npm install --save multer
RUN npm install swagger-ui-express
RUN npm install swagger-jsdoc
# If you are building your code for production
EXPOSE 3000
CMD [ "nodemon","start"]
